# Target Group Module

This module is capable to generate a target group and bind it to an existing application load balancer

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `port` - usage port
- `protocol` - usage protocol (default HTTP)
- `vpc_id` - vpc identifier
- `healthcheck_path` - healthcheck path
- `load_balancer_arn` - load balancer arn

Usage
-----

```hcl
module "target_group" {
  source            = "git::https://gitlab.com/mesabg-tfmodules/target_group.git"

  environment       = "environment"
  name = {
    slug            = "slugname"
    full            = "Full Name"
  }

  port              = 30050
  protocol          = "HTTP"
  vpc_id            = "vpc_id"
  healthcheck_path  = "/healthcheck"
  load_balancer_arn = "arn::some"
}
```

Outputs
=======

 - `target_group_arn` - Created target group resource
 - `listener` - Created listener resource


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
