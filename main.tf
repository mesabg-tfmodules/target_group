terraform {
  required_version = ">= 0.13.2"
}

resource "aws_lb_target_group" "lb_target_group" {
  name          = var.name.slug
  port          = var.port
  protocol      = var.protocol
  vpc_id        = var.vpc_id

  dynamic "health_check" {
    for_each    = var.protocol != "TCP" ? [1] : []
    content {
      path      = var.healthcheck_path
    }
  }

  tags = {
    Name        = var.name.full
    Environment = var.environment
  }
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn   = var.load_balancer_arn
  port                = var.port
  protocol            = var.protocol // "HTTP"

  default_action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.lb_target_group.arn
  }
}
