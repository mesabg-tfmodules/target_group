variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type = object({
    slug      = string
    full      = string
  })
  description = "General load balancer name"
}

variable "port" {
  type        = number
  description = "Port to be used"
}

variable "protocol" {
  type        = string
  description = "Protocol to be used"
  default 	  = "HTTP"
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier"
  default     = null
}

variable "healthcheck_path" {
  type        = string
  description = "Healthcheck path"
  default     = "/healthcheck"
}

variable "load_balancer_arn" {
  type        = string
  description = "Load Balancer ARN"
}
