output "target_group_arn" {
  value       = aws_lb_target_group.lb_target_group.arn
  description = "Created Target Group ARN"
}

output "listener" {
  value       = aws_lb_listener.lb_listener
  description = "Created Listener"
}
